import subprocess
from prometheus_client import CollectorRegistry, Gauge, push_to_gateway, Counter

def ifHCIn():
    ifHCInOctets = subprocess.Popen('snmpwalk -c public -v 2c 163.18.26.250 IF-MIB::ifHCInOctets', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    x = ifHCInOctets.stdout.readlines()
    y = list()
    del x[53:60]
    del x[0:16]
    del x[21:37]
    del x[2]
    del x[3]
    del x[3]
    del x[4]
    del x[5]
    del x[6]
    del x[9]
    del x[10]
    del x[10]
    del x[10]
    del x[-1]
    for i in range(len(x)):
        x[i] = x[i].decode().strip()
        x[i] = x[i].split(':')
        y.append(x[i][3])
    for i in range(len(y)):
        y[i] = int(y[i])
    return y

def ifHCOut():
    ifHCOutOctets = subprocess.Popen('snmpwalk -c public -v 2c 163.18.26.250 IF-MIB::ifHCOutOctets', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    x = ifHCOutOctets.stdout.readlines()
    y = list()
    del x[53:60]
    del x[0:16]
    del x[21:37]
    del x[2]
    del x[3]
    del x[3]
    del x[4]
    del x[5]
    del x[6]
    del x[9]
    del x[10]
    del x[10]
    del x[10]
    del x[-1]
    for i in range(len(x)):
        x[i] = x[i].decode().strip()
        x[i] = x[i].split(':')
        y.append(x[i][3])
    for i in range(len(y)):
        y[i] = int(y[i])
    return y

def pushOUT(port, MAC, value):
    registry = CollectorRegistry()
    g = Gauge('pyOut', 'Last time a batch job successfully finished', registry=registry,labelnames=['instance','portnum','MACaddr'])
    g.labels(instance='163.18.26.250',portnum=port,MACaddr=MAC).inc(value)
    push_to_gateway('localhost:9091', job="out:"+port, registry=registry)

def pushIN(port, MAC, value):
    registry = CollectorRegistry()
    g = Gauge('pyPortToMac', 'Last time a batch job successfully finished', registry=registry,labelnames=['instance','portnum','MACaddr'])
    g.labels(instance='163.18.26.250',portnum='1:'+port,MACaddr=MAC).inc(value)
    push_to_gateway('localhost:9091', job=port, registry=registry)

ports = ['17','18','20','25','27','29','30','31','33']
MAC = list()
for port in ports:
    portsnmp = subprocess.Popen('snmpwalk -v 1 -c public 163.18.26.250 1.3.6.1.2.1.17.4.3.1.2 | grep "INTEGER: '+port+'"', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    output1 = portsnmp.stdout.readlines()
    tmp1 = output1.pop().decode()
    a = tmp1.split('.')
    b = a[-1].split(' ')
    c = ""
    for i in range(11,16):
        c+=(a[i]+'.')
    c+=b[0]
    macsnmp = subprocess.Popen('snmpwalk -v 1 -c public 163.18.26.250 1.3.6.1.2.1.17.4.3.1.1 | grep '+c, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    output2 = macsnmp.stdout.readlines()
    tmp2 = output2[0].decode()
    d = tmp2.split(' ')
    e = ""
    for i in range(3,8):
        e+=(d[i]+':')
    e+=d[8]
    MAC.append(e)
for j in range(len(ports)):
    pushIN(ports[j],MAC[j],1)
#    pushOUT(ports[j],MAC[j],1)