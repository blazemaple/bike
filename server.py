import socket
import geohash2
import threading
import time
from prometheus_client import CollectorRegistry, Gauge, push_to_gateway
import datetime
now=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
rec=""

def push(value):
    global locate, id, now
    registry = CollectorRegistry()
    g = Gauge('location', 'SOS', registry=registry,labelnames=['instance','geohash','UUID','time'])
    g.labels(instance='bike123', geohash=locate, UUID=id+"  "+now, time=now).inc(value)
    push_to_gateway('localhost:9091', job='pushgateway', registry=registry)

def check():
    global rec
    while True:
        time.sleep(10)
        if rec=="":
            push(0)
t = threading.Thread(target = check)
x=1
PORT = 5000
s.bind(('0.0.0.0', PORT))
while True:
    rec=""
    print('Listening for broadcast at ', s.getsockname())
    mes, address = s.recvfrom(65535)
    print('Server received from {}:{}'.format(address, mes.decode('utf-8')))
    rec=mes.decode('utf-8', 'ignore').strip().strip(b'\x00'.decode())
    data=rec.split(',')
    id=data[0]
    maj=data[1]
    lat=float(data[2])
    lot=float(data[3])
    print('Geohash:', geohash2.encode(lot,lat))
    locate=str(geohash2.encode(lot,lat))
    push(10)
    
    if x==1:
        t.start()
        x=0
