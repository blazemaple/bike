import geohash2
from prometheus_client import CollectorRegistry, Gauge, push_to_gateway
import datetime
now=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
print(now)
rec="db1513143217534f53fcdf3009875e61f050000ac3,050,120.300922,22.776151"
data=rec.split(',')
id=data[0]
maj=data[1]
lat=float(data[2])
lot=float(data[3])
print('Geohash:', geohash2.encode(lot,lat))
locate=str(geohash2.encode(lot,lat))
registry = CollectorRegistry()
g = Gauge('location', 'SOS', registry=registry,labelnames=['instance','geohash','UUID','time'])
g.labels(instance='bike123', geohash=locate, UUID=id+"  "+now, time=now).inc(10)
push_to_gateway('localhost:9091', job='pushgateway', registry=registry)